class ArticlesController < ApplicationController
  before_action :authenticate_user!, only: %i[new create edit update destroy] 

  def index
    authorize Article
    @articles = Article.ordered.with_authors
    @articles = Article.paginate(:page => params[:page], per_page:2)
  end
 
  def show
    @article = Article.find(params[:id])
    authorize @article
  end

  def new
    authorize Article
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)
    authorize Article
    @article.author = current_user
    if @article.save
      redirect_to @article
    else
      render :new
    end
  end

  def edit
    @article = Article.find(params[:id])
    authorize @article
  end

  def update
    @article = Article.find(params[:id])
    authorize @article
    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
  end

  def destroy
    @article = Article.find(params[:id])
    authorize @article
    @article.destroy

    redirect_to root_path
  end

  private
  def article_params
    params.require(:article).permit(:title, :body, :status, :image)
  end
end
